package com.example.conversormonedas;

public class Conversor {
    private double factor;

    public Conversor() {
        this.factor = 0.85;
    }

    public Conversor(Double factor){
        this.factor = factor;
    }

    public String convertToDollar(String cantidad) throws NumberFormatException{
        Double valor = Double.parseDouble(cantidad) * factor;
        return String.format("%.2f", valor);
    }

    public String convertToEuro(String cantidad) throws NumberFormatException{
        Double valor = Double.parseDouble(cantidad) / factor;
        return String.format("%.2f", valor);
    }
}
