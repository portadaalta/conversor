package com.example.conversormonedas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity /*implements View.OnClickListener*/ {

    private final double FACTOR = 0.85;
    private Conversor conversor;
    private EuroTextChangedListener euroTextChangedListener = new EuroTextChangedListener();
    private DollarTextChangedListener dollarTextChangedListener = new DollarTextChangedListener();

    @BindView(R.id.edEuro)
    EditText edEuro;

    @BindView(R.id.edDollar)
    EditText edDollar;

    @BindView(R.id.rdDolarEuro)
    RadioButton rdDolarEuro;

    @BindView(R.id.rdEuroDolar)
    RadioButton rdEuroDolar;

//    @BindView(R.id.btConvert)
//    Button btConvert;

    @BindView(R.id.rgConvert)
    RadioGroup rgConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        conversor = new Conversor(FACTOR);
        edEuro.addTextChangedListener(euroTextChangedListener);

        rgConvert.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (rgConvert.getCheckedRadioButtonId()){
                    case R.id.rdEuroDolar:
                        edDollar.setEnabled(false);
                        edEuro.setEnabled(true);
                        edEuro.requestFocus();
                        edEuro.addTextChangedListener(euroTextChangedListener);
                        edDollar.removeTextChangedListener(dollarTextChangedListener);
                        edEuro.setText(edDollar.getText());
                        if (!edEuro.getText().toString().isEmpty()){
                            edDollar.setText(conversor.convertToDollar(edEuro.getText().toString()));
                        }
                        break;
                    case R.id.rdDolarEuro:
                        edDollar.addTextChangedListener(dollarTextChangedListener);
                        edEuro.removeTextChangedListener(euroTextChangedListener);

                        edEuro.setEnabled(false);
                        edDollar.setEnabled(true);
                        edDollar.requestFocus();

                        edDollar.setText(edEuro.getText());
                        if (!edDollar.getText().toString().isEmpty()) {
                            edEuro.setText(conversor.convertToEuro(edDollar.getText().toString()));
                        }
                        break;
                }
            }
        });

//        btConvert.setOnClickListener(this);




//        btConvert.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                    try {
//                        if (!edEuro.getText().toString().isEmpty()) {
//                            if (rdDolarEuro.isChecked()) {
//                                ConvertirMoneda(factorDollarEuro, edEuro);
//                            } else if (rdEuroDolar.isChecked()) {
//                                ConvertirMoneda(factorEuroDollar, edDollar);
//                            }
//                        } else {
//                            mensaje = "Introduce una cantidad.";
//                            error = true;
//
//                        }
//                    } catch (Exception e){
//                        mensaje = "Introduce una cantidad válida, por favor.";
//                        error = true;
//                    } finally {
//                        if (error) {
//                            Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
//                        }
//                    }
//            }
//        });
    }

//    @Override
//    public void onClick(View view) {
//        boolean error = false;
//        StringBuilder mensaje = new StringBuilder();
//        if (view == btConvert) {
//            try {
//                if (rdDolarEuro.isChecked()){
//                    String valor = edDollar.getText().toString();
//                    if (valor.isEmpty()){
//                        edEuro.setText("");
//                    } else {
//                        edEuro.setText(conversor.convertToEuro(valor));
//                    }
//                } else {
//                    String valor = edEuro.getText().toString();
//                    if (valor.isEmpty()){
//                        error = true;
//                        mensaje.append("Debe introducir un valor.");
//                        edDollar.setText("");
//                    } else {
//
//                        edDollar.setText(conversor.convertToDollar(valor));
//                    }
//                }
//            } catch (NumberFormatException e) {
//                e.printStackTrace();
//                Log.e("ERROR", e.getMessage() + "\n" + e.getCause());
//                error = true;
//                mensaje.append(e.getMessage());
//            } finally {
//                if (error) {
//                    Toast.makeText(getApplicationContext(), mensaje.toString(), Toast.LENGTH_LONG).show();
//                }
//            }
//
//        }
//    }

    private class EuroTextChangedListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().isEmpty()){
                edDollar.setText("");
            } else {
                try {
                    edDollar.setText(conversor.convertToDollar(edEuro.getText().toString()));
                } catch (NumberFormatException e) {
                    edEuro.setText("");
                }
            }
        }
    }

    private class DollarTextChangedListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().isEmpty()){
                edEuro.setText("");
            } else {
                try {
                    edEuro.setText(conversor.convertToEuro(edDollar.getText().toString()));
                } catch (NumberFormatException e) {
                    edDollar.setText("");
                }
            }
        }
    }
}
